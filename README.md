# MFRC522


MFRC522 is a spin object that interfaces the RC522 Based RFID Readers with the Propeller 2 P2X8C4M64P

Learn more about the Propeller 2 at [www.parallax.com](http://www.parallax.com).

## Known Issues
Driver is a work in progress. 
Driver is functionally complete.  I dont have all card types so more testing is needed

## License

Copyright © 2022 Greg LaPolla MFRC522 Spin 2 Object is licensed under the MIT License.

## Credits

MFRC522 Spin 2 Object is developed by Greg LaPolla.
 
This Driver was derived from The Arduino Library MFRC522.c by Dr.Leong, Miguel Balboa, Søren Thing Andersen, Tom Clement, many more!                   
